extern crate nom;
extern crate xml;
extern crate docopt;
extern crate flate2;

use std::fs::File;
use std::io::BufReader;

use xml::reader::{EventReader, XmlEvent};

use docopt::Docopt;
use serde::Deserialize;

use flate2::read::GzDecoder;


const USAGE: &'static str = "
Compressed Protein Coordinate DataBase (cpcdb)

Usage:
  cpcdb FILE
  cpcdb (-h | --help)
  cpcdb --version

Options:
  -h --help    Show this help and exit.
  --version    Show version.
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_FILE: String,
}

fn indent(size: usize) -> String {
    const INDENT: &'static str = "    ";
    (0..size).map(|_| INDENT)
             .fold(String::with_capacity(size*INDENT.len()), |r, s| r + s)
}


fn main() -> Result<(), std::io::Error> {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());
    println!("{:?}", args);
    let f = File::open(args.arg_FILE)?;
    let data = GzDecoder::new(f);

    let parser = EventReader::new(data);
    let mut depth = 0;
    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, .. }) => {
                println!("{}+{}", indent(depth), name);
                depth += 1;
            }
            Ok(XmlEvent::EndElement { name }) => {
                depth -= 1;
                println!("{}-{}", indent(depth), name);
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }

    Ok(())
}
